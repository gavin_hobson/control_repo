class profile::ssh_server {
  package {'openssh-server':
    ensure => present,
  }
  service { 'sshd':
    ensure => 'running',
    enable => 'true',
  }
  ssh_authorized_key {'root@master.puppet.vm':
    ensure => present,
    user   => 'root',
    type   => 'ssh-rsa',
    key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCkYKTFcodTQDPgjE+l/z5D7cDgo/GlLXuEX33ZBQfSnCkjg6Kg9P1Ec+F5I+nQ1VjJze0dpHBpx3klkBDkBqNlNyiOOo909o4nitYzm6Mbg9UYE5DntSFjBR4BuDxPMAJirsTyvS++BGFRMr4QHz8ImBvsIQGFYnDOe1Ip54Z2b/PaEX/z1Bc7d9tCzLcySGwe2j7qIwExJ3SK9LLAzzap4DHvfS0DMT/Ia5q62O6Lj96W7VTrt8piZYSdvHMi/dQ5C+p/KhK0XHfZjTl1c4+PfbDbzMdzT61iUZriTHdVIqFyBRWS7MkGVVP5BciSx2mdlrYTRTxZvTFdZNls3CNr',
  }  
}
